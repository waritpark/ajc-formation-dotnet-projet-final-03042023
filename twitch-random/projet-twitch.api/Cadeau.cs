﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_twitch.api
{
	/// <summary>
	/// Cadeaux pour les abonnés
	/// </summary>
	public class Cadeau
	{
        #region Properties
        public int Id { get; set; }
        public string NomCadeau { get; set; }
		#endregion

		#region Constructor
		public Cadeau()
        {
            
        }
		public Cadeau(int id, string nomCadeau)
		{
			Id = id;
			NomCadeau = nomCadeau;
		}
		#endregion
	}
}
