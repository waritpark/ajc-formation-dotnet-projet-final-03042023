﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_twitch.api.Adapters
{
	/// <summary>
	/// Contrat à implémenter pour sauvegarder les followers et les cadeaux
	/// </summary>
	public interface ITwitchSave
	{
		/// <summary>
		/// Liste des followers
		/// </summary>
		/// <param name="commu"></param>
		void SaveCommunaute(List<Communaute> commu);

		/// <summary>
		/// Liste des Cadeaux
		/// </summary>
		/// <param name="kdo"></param>
		void SaveCadeau(List<Cadeau> kdo);
	}
}
