﻿using projet_twitch.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projet_twitch.api
{
	/// <summary>
	/// Permet de sauvegarder une liste de cadeau
	/// </summary>
	public class CadeauList
	{
		#region Properties
		private List<Cadeau> List { get; set; }
		#endregion

		#region Fields
		private readonly ITwitchSave saver;
        #endregion

        #region Constructor
        /// <summary>
        /// Passe en param l'interface pour sauvegarder la liste de cadeaux
        /// </summary>
        /// <param name="saver"></param>
        public CadeauList(ITwitchSave saver)
		{
			this.saver = saver;
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Ajoute une liste de cadeaux
		/// </summary>
		/// <param name="list"></param>
		public void AddListCadeau(List<Cadeau> list)
		{
			this.List = list;
		}

		/// <summary>
		/// Sauvegarde de la liste de cadeaux
		/// </summary>
		public void SaveListCadeau()
		{
			this.saver.SaveCadeau(this.List);
		}
		#endregion
	}
}
