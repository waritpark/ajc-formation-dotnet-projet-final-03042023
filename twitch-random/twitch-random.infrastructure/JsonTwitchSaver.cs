﻿using Newtonsoft.Json;
using projet_twitch.api;
using projet_twitch.api.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch_random.infrastructure
{
	/// <summary>
	/// Sauvegarder en json
	/// </summary>
	public class JsonTwitchSaver : ITwitchSave
	{
		#region Fields
		private readonly string chemin;
		#endregion

		#region Constructor
		/// <summary>
		/// Passe en param le path du dossier pour l'emplacement du fichier 
		/// </summary>
		/// <param name="chemin"></param>
		public JsonTwitchSaver(string chemin)
		{
			this.chemin = chemin;
		}
		#endregion

		#region Public Methods
		/// <summary>
		/// Liste de la communauté
		/// </summary>
		/// <param name="commu"></param>
		public void SaveCommunaute(List<Communaute> commu)
		{
			this.Save(commu);
		}

		/// <summary>
		/// Liste des cadeaux
		/// </summary>
		/// <param name="kdo"></param>
		public void SaveCadeau(List<Cadeau> kdo)
		{
			this.Save(kdo);
		}
		#endregion

		#region Private Methods
		private void Save(object objt)
		{
			string json = JsonConvert.SerializeObject(objt);
			File.WriteAllText(this.chemin, json);
		}
		#endregion
	}
}
