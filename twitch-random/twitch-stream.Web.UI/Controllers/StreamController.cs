﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing.Matching;
using System.Diagnostics;
using twitch_stream.Models;
using projet_twitch.api;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace twitch_stream.Controllers
{
    public class StreamController : Controller
    {
        private readonly DefaultDbContext context;

        public StreamController(DefaultDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var query = from item in this.context.Streamers
                        select item;

            List<Streamer> list = query.ToList();

            return View(list);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(StreamerAddViewModel item)
        {
            if (!ModelState.IsValid)
            {
                ViewData["message"] = "Ajout du stream échoué !";
                return View();
            }
            else
            {
                this.context.Streamers.Add(item.UnStreamer);
                this.context.SaveChanges();
                TempData["message"] = "Stream ajouté !";
                return RedirectToAction("Index", TempData["message"]);
            }
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            //Streamer entity = this.context.Streamers.Find(id);
            //return View(entity);
            Streamer entity = this.context.Streamers.Find(id);
            return View(entity);

        }

        [HttpPost]
        public IActionResult Edit(Streamer Stm)
        {
            if (!ModelState.IsValid)
            {
                ViewData["message"] = "Modification du stream échoué !";
                return View();
            }
            else
            {
                var streamer = new Streamer()
                {
                    Id = Stm.Id,
                    DateS = Stm.DateS,
                    Title = Stm.Title,
                    Statut = Stm.Statut
                };
                this.context.Streamers.Update(streamer);
                this.context.SaveChanges();
                TempData["message"] = "Modification du stream effectué !";
                return RedirectToAction("Index", TempData["message"]);
            }
        }
    }
}