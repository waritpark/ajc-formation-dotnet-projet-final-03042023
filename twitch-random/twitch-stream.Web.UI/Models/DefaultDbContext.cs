﻿using Microsoft.EntityFrameworkCore;
using projet_twitch.api;

namespace twitch_stream.Models
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }

        /// <summary>
        /// C'est une LISTE ! et elle sera connectée à une table en bdd
        /// </summary>
        public DbSet<Streamer> Streamers { get; set; }
    }
}
