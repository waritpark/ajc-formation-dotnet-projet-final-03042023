﻿namespace menu.api
{
    public class Menu
    {
        #region Fields
        private readonly Action<string> afficherInfo;
        private readonly Func<string> recupererInfo;
        #endregion

        #region Properties
        public List<MenuItem> Items { get; private set; } = new();
        #endregion

        #region Constructors
        public Menu(Action<string> afficher, Func<string> recupererInfo)
        {
            this.afficherInfo = afficher;
            this.recupererInfo = recupererInfo;
        }
        #endregion

        #region Public methods
        public void Afficher()
        {
            foreach (var item in this.Items.OrderBy(item => item.OrdreAffichage))
            {
                this.afficherInfo(item.ToString());
            }
        }

        public void Ajouter(MenuItem item)
        {
            this.Items.Add(item);
        }
        #endregion

    }

}