﻿using projet_twitch.api;

namespace twitch.projet.Web.UI.Models
{
    public class FollowListViewModel
    {
        public List<Follow> FollowList { get; set; } = new();
    }
}
