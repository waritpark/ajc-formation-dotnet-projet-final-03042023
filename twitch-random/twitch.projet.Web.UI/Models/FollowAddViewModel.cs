﻿using projet_twitch.api;

namespace twitch.projet.Web.UI.Models
{
    public class FollowAddViewModel
    {
        public Follow UnFollow { get; set; } = null!;
    }
}
