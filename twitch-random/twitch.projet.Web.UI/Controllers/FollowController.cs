﻿using Microsoft.AspNetCore.Mvc;
using projet_twitch.api;
using System.Linq;
using twitch.projet.Web.UI.Models;

namespace twitch.projet.Web.UI.Controllers
{
    public class FollowController : Controller
    {
        private readonly DefaultDbContext context;
        public FollowController(DefaultDbContext context)
        {
            this.context = context;
        }

        public IActionResult List()
        {
            return View();
        }

        [HttpPost]
        public IActionResult List(string followList)
        {
            var queryFollow = from follower in this.context.Follows
                                select follower;
            var queryFollowPresent = from follow in this.context.Follows
                                        where follow.PresenceTotal > 7
                                        select follow;

            List<Follow>? listFollow = null;

            if (followList == "all")
            {
                listFollow = queryFollow.ToList();
            }
            else
            {
                listFollow = queryFollowPresent.ToList();
            }
            return View("List", listFollow);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(FollowAddViewModel item)
        {
            if (ModelState.IsValid!)
            {
                ViewData["message"] = "Ajout du follower échoué !";
                return View();
            }
            else
            {
                this.context.Follows.Add(item.UnFollow);
                if (item.UnFollow.ImageProfil == null)
                {
                    item.UnFollow.ImageProfil = "https://ddragon.leagueoflegends.com/cdn/img/champion/splash/Shen_0.jpg%27";
                }
                else
                {

                }
                this.context.SaveChanges();
                ViewData["message"] = "Ajout du follower réussi !";
                return View("List");
            }

        }
    }
}
